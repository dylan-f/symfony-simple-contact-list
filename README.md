A Symfony project created on February 25, 2016, 10:30 am.

FA e-Services - Dylan FOREST

Application de gestion de contact dans laquelle on peut retrouver.

- restfull controller avec gestion d'erreur (Model annotations)
- envoi de mail à l'administrateur à l'ajout (ContactController.newAction()) contenant nom et prénom (mail text/html, donc utilisation d'un template twig registration.html.twig)
- utilisation de mailtrap et d'un compte personnel pour récupérer les mails (modification dans config.yml -> switfmailer)